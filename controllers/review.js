var models = require('../database');

// Create and save a new review for a dish
exports.create = function(req, res){
	
	req.accepts('application/json');
	req.accepts('application/x-www-form-urlencoded');
	
	// Make sure we are receiving POST data and the sender is signed in
	if(req.method === "POST" && typeof req.session.user !== "undefined" && (req.session.level == 1 || req.session.level > 2)){
		
		console.log("Attempting to save a new review");
		
		var review = null;
		
		
		if(req.is('application/json')){
		
			review = req.body.review;
			review.user = req.session.user;
		
			if(saveReview(review)){
				console.log("Review saved!");
				res.json(review);
			}else{
				console.log("Saving review failed!");
				res.json(review);
			}		

		}else if(req.is('application/x-www-form-urlencoded')){
		
			review = {
				dish: req.body.dishId,
				user: req.session.user,
				rating: req.body.ratingValue,
				text: req.body.reviewText
			};	
		
			if(saveReview(review)){
				res.redirect("/dish/"+review.dish);
			}else{
				res.redirect("/dish/"+review.dish);
			}		
		}

		
	}else{
		if(req.is('application/json')){
			res.json('{error:"review not saved"}');
		}else if(req.is('application/x-www-form-urlencoded')){
				res.redirect("/dish/"+review.dish);
		}else{
			res.send("New reviews require the use of HTTP POST");
		}
		
	}
};


function saveReview(review){
	var newReview = new models.Review(review);
	
    newReview.save(function(error) {
      if (error) {
		  console.log(error);
		  return false;      
      }else{
      	  // If all goes well, we save the stats too
		  models.Dish.findById(review.dish, function (err, dish){
			  if(err){
				  console.log(error);
				  return false;     
			  }else{
				  // Add review points to total dish points
				  // TODO: For some reason the rating is string, so parseInt is needed
				  dish.points = dish.points + parseInt(review.rating);
				  // Increment the amount of reviews
				  dish.reviews = dish.reviews + 1;
					dish.rating = dish.points / dish.reviews;
				  
				  dish.save(function(err){
					  if (err) return false;
				  });
			  }
		  });
      }
     
    });
	
	return true;
};

exports.update = function(req, res) {

    if(req.method === "GET") {
    	console.log("Requested review with id: "+req.param('id'));

   		models.Review.findOne({'_id':req.param('id')}, function(err, review) {
        
        	if(err) {

        		throw err;
        	}
        	else {

                if(review !== null) {
        		     res.redirect("/review/show", {reviewdata:review}, {mode:'UPDATE'});

        			/*if(review.user._id === req.session.user) {

        				
        			}
        			else {

        				console.log("User: "+review.user.username+" tried to edit review: "+review._id);
        			}*/
        		}
        		else {
        			console.log("There");
        			res.redirect("/review/show");
        		}
        	}
        	
    	});
	
	}
	else {


	}
	
};


// Get information of all reviews of a particular dish
exports.all = function(req, res){
  res.send("nothing here yet");
};