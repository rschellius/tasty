/**
  Tasty App Server
  ****************

  This file contains app environment set up and not much else,
  it's not a good idea to grow this file.

  Project Structure:

  - app dependencies are handled at package.json -file
  - database-settings and configuration is done in database.js
  - context routing is done in router.js
  - route controllers and their logic are in the controller-directory
  - views are created with jade at view-directory, each controller has it's own directory
  - styling is done with stylus at public/stylesheets-directory, please don't use inline CSS
*/


/**
 * Module dependencies.
*/

var express = require('express');
var http = require('http');
var path = require('path');
var db = require('./database');
var crypto = require('crypto');
var app = express();

// all environments
app.set("title", "Tasty Dish Reviews");
app.set("approot", __dirname);
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.multipart());
app.use(express.cookieParser());
app.use(express.session({secret: "TastyWebPageIsTasty"}));

// Local variables for editing layout.jade content.
app.use(function(req, res, next) {
  res.locals.session = req.session;
  next();
});

app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Routing happens in router.js file
require("./router.js")(app);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
