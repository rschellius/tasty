$(document).ready(function(req, res) {

  $("#ajax_loader").hide();     //for throbber.gif ajax indicator

  //$(".collapsible").jPanel();    //close password changing div as default, toggle link

  $("#restaurant_fields").hide();

  $("#cancel_review_btn").click(function() {

      $("#reviewText").val("");
      $("#ratingValue").val("");

      $(".edit_review").hide();
  });

  $(".rateit").bind('rated', function (event, value) {
		$("#ratingValue").val(value);

  });

  $("#edit_review_form").submit(function(event) {
      event.preventDefault();

      var review = {
      		rid: $("#reviewid").val(),
			rating: $("#ratingValue").val(),
			text: $("#reviewText").val()
		};
		// Make an ajax call and send the review as JSON
		$.ajax({
		  url: "/dashboard/editreview",
		  type: "POST",
		  contentType: "application/json; charset=utf-8",
		  dataType: "json",
		  data: JSON.stringify({ review: review }),

		  success: function(data) {
		  	 if(data.status === "SUCCESS") {

		  	 	Notifier.success("Review updated");

		  	 	$("#ratingValue").val(0);
			    $("#reviewText").val("");


		  	 }
		  }
		});
  });

  $("#restaurant").click(function() {

  	 var isChecked = $('#restaurant').prop('checked');
  	 if(isChecked)
         $("#restaurant_fields").show();
     else
     	 $("#restaurant_fields").hide();
  });

  $("#password_change_form").submit(function(event) {

        event.preventDefault();

        var pword = $("#password_recent").val();
        var pw1 = $("#password_new1").val();
        var pw2 = $("#password_new2").val();
        var json = {password : pword};
        var passwordRegEx = /(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/;

        $.ajax({

        	url: "/dashboard/passwordRecent",
            type: 'POST',
            data: JSON.stringify(json),
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: function() {

            	$("#ajax_loader").show();

            },
            success: function(data) {
            	if(data.status == "ERROR") {

            		$("#ajax_loader").hide();

            		Notifier.error("Recent password is not valid");
         			$("#password_recent").val("");

                }
                else {

                	if(pw1 !== pw2) {
             			Notifier.error("Passwords dont match");
             			$("#password_new1").val("");
        				$("#password_new2").val("");

         			}
         			else if(!passwordRegEx.test(pw1) || !passwordRegEx.test(pw2)) {

         				Notifier.error("Passwords must be at least 8 characters long and include at least 1 number and 1 upper case letter");
         			}
         			else {

         				updatePasswords(pw1);
         			}
                }

            }

        });

 });
 });

function updatePasswords(password1) {

            var json = {password : password1};
      	    $.ajax({

        	    url: "/dashboard/updatePassword",
                type: 'POST',
                data: JSON.stringify(json),
                dataType: 'json',
                contentType: 'application/json',

                success: function(data) {

            	    if(data.status == "SUCCESS") {

            		     $("#ajax_loader").hide();
            		     Notifier.success("Password updated successfully");

            		     $("#password_new1").val("");
      					 $("#password_new2").val("");
      					 $("#password_recent").val("");
                    }
                }

            });
}

function handleActivation(id, rowindex, mode) {

            if(mode == 1)
              var activate = true;
            else
              var activate = false;

            var json = {id : id, active:activate};
            $.ajax({

              url: "/dashboard/handleactivate",
                type: 'POST',
                data: JSON.stringify(json),
                dataType: 'json',
                contentType: 'application/json',

                success: function(data) {

                  if(data.status == "SUCCESS") {

                       if(mode == 1) {
                          var html = "<a href='#' id="+id+" onclick='handleActivation(this.id, "+rowindex+", -1);'>";
                          html += "<img src='/images/active.png' alt='Active restaurant' title='Active'></img>";
                          html += "</a>";

                          $("#user_table > tbody > tr").eq(rowindex).children('td:last').html(html);
                       }
                       else {
                          var html = "<a href='#' id="+id+" onclick='handleActivation(this.id, "+rowindex+", 1);'>";
                          html += "<img src='/images/inactive.png' alt='Inactive restaurant' title='Inactive'></img>";
                          html += "</a>";

                          $("#user_table > tbody > tr").eq(rowindex).children('td:last').html(html);

                       }
                  }
                }

            });
}


function convertDate(str){

  var d = new Date(str);
  return addZero(d.getDate())+"."+addZero(d.getMonth()+1)+"."+d.getFullYear();
}

//If we have the number 9, display 09 instead
function addZero(num){
  return (num<10?"0":"")+num;
}


