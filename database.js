/*
	Database Configuration and Management
	*************************************

	Please create CRUD logic in model-files, this file just contains
	connection, schema and model creation. The business logic is handled
	at routes-directory's route-files.

	Initial database settings, schemas, models

*/

// Database settings
var mongoose = require('mongoose');
var fs = require('fs');
var uriString = "mongodb://localhost/tastydb";
var textSearch = require('mongoose-text-search');

// Connect to database
mongoose.connect(uriString, function(err,db)
{
    //if we failed to connect
    if(err)
    {
        console.log('Could not connect to database');
    }
    else
    {
        console.log('Connected to database');
    }
});

// Create database schema

var Schema = mongoose.Schema;

// Create database models

var userSchema = new Schema(
{
   username:{type:String,unique:true, required:true},
   longname:{type:String},
   phonenumber:{type:String},
   street_address:{type:String},
   postal_code:{type:String},
   city:{type:String},
   country:{type:String, default:'Finland'},
   password:{type:String,required:true},
   password_salt:{type:String,required:true},
   email:{type:String,unique:true, required:true},
   photopath:{type:String},
   added:{type:Date, default: Date.now},
	 updated:{type:Date, default: Date.now},
   active:{type:Boolean},
   level:{type:Number,required:true}

});

var dishSchema = new Schema(
{
   name:{type:String,required:true},
   description:{type:String,required:true},
   price:{type:Number},
   type:{type:String,required:true},
   keywords:[{type:String}],
   photopath:{type: String},
	 points:{type:Number, default: 0}, // agregate data from reviews
	 reviews:{type:Number, default: 0},// agregate data from reviews
	 rating:{type:Number, default: 0},// agregate data from reviews
	 highlighted:{type:Boolean, default: false}, // this is used to mark noteworthy dishes
   added:{type:Date, default: Date.now},
   updated:{type:Date, default: Date.now},
   restaurant:{type:Schema.Types.ObjectId, ref:'User'}

});

dishSchema.plugin(textSearch)
dishSchema.index({ "$**":'text'});

var reviewSchema = new Schema(
{
   rating:{type:Number},
   text:{type:String},
   added:{type:Date, default: Date.now},
   updated:{type:Date, default: Date.now},
   user:{type:Schema.Types.ObjectId, ref:'User'},
   dish:{type:Schema.Types.ObjectId, ref:'Dish'}
});



// Export models
var User = mongoose.model('User', userSchema);
var Dish =  mongoose.model('Dish', dishSchema);
var Review = mongoose.model('Review', reviewSchema);

exports.User = User;
exports.Dish = Dish;
exports.Review = Review;
